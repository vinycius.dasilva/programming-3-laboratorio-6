﻿using System.Diagnostics;
using System.Reactive;
using System.Threading.Tasks;
using ReactiveUI;

namespace B._6_Laboratório_Semana_6.ViewModels;

public class MainWindowViewModel : ReactiveObject
{
    public MainWindowViewModel()
    {
        ExecuteTopButton = _executeTopButton();
        ExecuteBottomCommand = _executeBottomButton();
    }

    private string _topText;

    public string TopText
    {
        get => _topText;
        set => this.RaiseAndSetIfChanged(ref _topText, value);
    }

    private string _bottomText;

    public string BottomText
    {
        get => _bottomText;
        set => this.RaiseAndSetIfChanged(ref _bottomText, value);
    }

    public ReactiveCommand<Unit, Task> ExecuteTopButton { get; }

    public ReactiveCommand<Unit, Task> ExecuteBottomCommand { get; }

    private ReactiveCommand<Unit, Task> _executeTopButton()
    {
        return ReactiveCommand.Create(async () =>
        {
            TopText = "Iniciando Processamento sequencial\n";
            var watch = Stopwatch.StartNew();

            for (var i = 0; i < 10; i++)
            {
                await ProcessTaskAsync();
                TopText += $"Tarefa {i} concluída.\n";
            }

            watch.Stop();
            TopText += $"Processamento sequencial concluído em {watch.ElapsedMilliseconds} ms.";
        });
    }

    private ReactiveCommand<Unit, Task> _executeBottomButton()
    {
        return ReactiveCommand.Create(async () =>
        {
            BottomText = "Iniciando processamento paralelo...";
            var watch = Stopwatch.StartNew();

            var tasks = new Task[10];

            for (var i =  0; i <  10; i++)
            {
                tasks[i] = ProcessTaskAsync();
                BottomText += $"Tarefa {i} concluída.\n";
            }

            await Task.WhenAll(tasks);

            watch.Stop();
            BottomText += $"Processamento paralelo concluído em {watch.ElapsedMilliseconds} ms.";
        });
    }

    private static async Task ProcessTaskAsync()
    {
        await Task.Delay(1000);
    }
}